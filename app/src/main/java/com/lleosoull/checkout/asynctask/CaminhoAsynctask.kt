package com.lleosoull.checkout.asynctask

import com.lleosoull.checkout.room.entity.CartaoModel

interface CaminhoAsynctask {

    fun bloco(usuarios: List<CartaoModel>)
}
