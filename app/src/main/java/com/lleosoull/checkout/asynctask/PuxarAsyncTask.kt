package com.lleosoull.checkout.asynctask

import android.os.AsyncTask
import android.util.Log
import com.lleosoull.checkout.room.dao.CartaoDAO
import com.lleosoull.checkout.room.entity.CartaoModel

class PuxarAsyncTask(private val cartaoDAO: CartaoDAO, private val caminho: CaminhoAsynctask) :
    AsyncTask<CartaoModel, Void, List<CartaoModel>>() {

    private var bloco: List<CartaoModel>? = null

    override fun doInBackground(vararg p0: CartaoModel): List<CartaoModel>? {
        Log.i("CartaoAsynctask", "Está puxando infomrção")
        bloco = cartaoDAO.getAll()
        return bloco
    }

    override fun onPostExecute(result: List<CartaoModel>?) {
        super.onPostExecute(result)

        if (result != null) {
            Log.i("Asynctask", "Enviando informação para meu callback")
            caminho.bloco(result)

            Log.i("CartaoAsynctask", "Sucesso!")
        }
    }

}