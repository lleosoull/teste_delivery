package com.lleosoull.checkout.asynctask

import android.os.AsyncTask
import android.util.Log
import com.lleosoull.checkout.room.dao.CartaoDAO
import com.lleosoull.checkout.room.entity.CartaoModel

class InserirAsynctask(private val cartaoDAO: CartaoDAO, private val cartaoModel: CartaoModel) :
    AsyncTask<CartaoModel, Void, Void>() {

    override fun doInBackground(vararg p0: CartaoModel?): Void? {
        Log.i("CartaoAsynctask", "Foi inserido irmao")

        Log.i(
            "CartaoAsynctask", "numero = ${cartaoModel.cartao_numero} \n" +
                    "nome = ${cartaoModel.cartao_user} \n" +
                    "validade = ${cartaoModel.cartao_validade} \n" +
                    "CVV = ${cartaoModel.cartao_cvv}"
        )

        cartaoDAO.insert(cartaoModel)
        return null
    }
}