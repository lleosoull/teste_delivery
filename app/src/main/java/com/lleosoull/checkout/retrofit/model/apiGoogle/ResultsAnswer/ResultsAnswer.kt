package com.lleosoull.checkout.retrofit.model.apiGoogle.ResultsAnswer

import com.google.gson.annotations.SerializedName

data class ResultsAnswer(

    @SerializedName("formatted_address") var formatted_address: String
)