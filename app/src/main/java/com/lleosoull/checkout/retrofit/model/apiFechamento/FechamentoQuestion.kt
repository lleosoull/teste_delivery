package com.lleosoull.checkout.retrofit.model.apiFechamento

import com.lleosoull.checkout.util.Util

class FechamentoQuestion {

    val store_latitude: Double = Util.latitudeDestino!!
    val store_longitude: Double = Util.longitudeDestino!!
    val user_latitude: Double = Util.latitudeOrigin!!
    val user_longitude: Double = Util.longitudeOrigin!!
    val card_number: String = Util.cartao_numero!!
    val cvv: String = Util.cartao_CVV!!
    val expiry_date: String = Util.cartao_validade!!
    val value: Double = Util.valorInstimulado!!.toDouble()
}