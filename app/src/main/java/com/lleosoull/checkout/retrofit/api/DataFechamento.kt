package com.lleosoull.checkout.retrofit.api

import com.lleosoull.checkout.retrofit.model.apiFechamento.FechamentoAnswer
import com.lleosoull.checkout.retrofit.model.apiFechamento.FechamentoQuestion
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface DataFechamento {

    @POST("order")
    fun addFechamento(
        @Body fechamentoQuestion: FechamentoQuestion
    ): Call<FechamentoAnswer>
}