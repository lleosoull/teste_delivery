package com.lleosoull.checkout.retrofit.api

import com.lleosoull.checkout.retrofit.model.apiGoogle.LocationAnswer
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface DataService {

    @GET("json")
    fun getLocation(
        @Query("latlng") latilong: String,
        @Query("key") key_api: String
    ): Call<LocationAnswer>
}