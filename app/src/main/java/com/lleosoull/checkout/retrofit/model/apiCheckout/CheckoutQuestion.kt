package com.lleosoull.checkout.retrofit.model.apiCheckout

import com.lleosoull.checkout.util.Util

class CheckoutQuestion {

    val store_latitude: Double = Util.latitudeDestino!!
    val store_longitude: Double = Util.longitudeDestino!!
    val user_latitude: Double = Util.latitudeOrigin!!
    val user_longitude: Double = Util.longitudeOrigin!!
    val value: Double = Util.valorInstimulado!!.toDouble()
}