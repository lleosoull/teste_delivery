package com.lleosoull.checkout.retrofit.model.apiGoogle

import com.google.gson.annotations.SerializedName
import com.lleosoull.checkout.retrofit.model.apiGoogle.ResultsAnswer.ResultsAnswer

data class LocationAnswer (

    @SerializedName("status") var status : String,
    @SerializedName("results") var results : List<ResultsAnswer>
)