package com.lleosoull.checkout.retrofit

import com.lleosoull.checkout.retrofit.api.DataCheckout
import com.lleosoull.checkout.retrofit.api.DataFechamento
import com.lleosoull.checkout.retrofit.api.DataService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitAll {

    private val retrofitLocation = Retrofit.Builder()
        .baseUrl("https://maps.googleapis.com/maps/api/geocode/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun dataService(): DataService = retrofitLocation.create(DataService::class.java)


    private val retrofitCheckout = Retrofit.Builder()
        .baseUrl("https://d9eqa4nu35.execute-api.sa-east-1.amazonaws.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun checkoutService(): DataCheckout = retrofitCheckout.create(DataCheckout::class.java)


    private val retrofitFechamento = Retrofit.Builder()
        .baseUrl("https://mdk3ljy26k.execute-api.sa-east-1.amazonaws.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun fechamentoService(): DataFechamento = retrofitFechamento.create(DataFechamento::class.java)
}