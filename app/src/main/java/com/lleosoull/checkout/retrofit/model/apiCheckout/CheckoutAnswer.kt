package com.lleosoull.checkout.retrofit.model.apiCheckout

import com.google.gson.annotations.SerializedName

data class CheckoutAnswer(

    @SerializedName("product_value") var product_value: Double,
    @SerializedName("distance") var distance: Double,
    @SerializedName("total_value") var total_value: Double,
    @SerializedName("fee_value") var fee_value: Double
)