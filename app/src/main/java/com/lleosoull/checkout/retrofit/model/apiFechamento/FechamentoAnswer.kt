package com.lleosoull.checkout.retrofit.model.apiFechamento

import com.google.gson.annotations.SerializedName

data class FechamentoAnswer(

    @SerializedName("message") var message: String,
    @SerializedName("value") var value: Double
)