package com.lleosoull.checkout.retrofit.api

import com.lleosoull.checkout.retrofit.model.apiCheckout.CheckoutAnswer
import com.lleosoull.checkout.retrofit.model.apiCheckout.CheckoutQuestion
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface DataCheckout {

    @POST("evaluate")
    fun addCheckout(
        @Body checkoutQuestion: CheckoutQuestion
    ): Call<CheckoutAnswer>
}