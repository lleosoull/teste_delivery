package com.lleosoull.checkout.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lleosoull.checkout.room.dao.CartaoDAO
import com.lleosoull.checkout.room.entity.CartaoModel


@Database(entities = [CartaoModel::class], version = 1)
abstract class CartaoDB : RoomDatabase() {

    abstract val cartaoDAO: CartaoDAO

    companion object {

        private var INSTANCE: CartaoDB? = null

        fun getBancoDado(context: Context): CartaoDB? {

            if (INSTANCE == null) {

                synchronized(CartaoDB::class.java) {

                    if (INSTANCE == null) {

                        INSTANCE = Room.databaseBuilder<CartaoDB>(
                            context.applicationContext,
                            CartaoDB::class.java,
                            "tarefa_location"
                        )
                            .fallbackToDestructiveMigration()
                            .build()

                    }
                }
            }
            return INSTANCE
        }
    }
}