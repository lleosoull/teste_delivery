package com.lleosoull.checkout.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "cartao_data")
class CartaoModel(@ColumnInfo var carta_adicionado: String? = null) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo
    var cartao_user: String? = null

    @ColumnInfo
    var cartao_numero: String? = null

    @ColumnInfo
    var cartao_validade: String? = null

    @ColumnInfo
    var cartao_cvv: String? = null

}