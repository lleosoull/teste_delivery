package com.lleosoull.checkout.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.lleosoull.checkout.room.entity.CartaoModel


@Dao
interface CartaoDAO {

    @Query("SELECT * FROM cartao_data")
    fun getAll(): List<CartaoModel>

    @Query("DELETE FROM cartao_data")
    fun delAll()

    @Query("SELECT * FROM cartao_data LIMIT 1")
    fun verificyExist(): CartaoModel

    @Insert
    fun insert(cartaoModel: CartaoModel)
}