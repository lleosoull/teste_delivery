package com.lleosoull.checkout.views

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.lleosoull.checkout.R
import com.lleosoull.checkout.util.Util
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val permissoes = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.lleosoull.checkout.R.layout.activity_main)
        button_main.setOnClickListener(this)

        if (!Places.isInitialized())
            Places.initialize(applicationContext, Util.API_KEY)

        // Crie uma nova instância do cliente do Places
        val placesClient = Places.createClient(this)

        // Inicialize o AutocompleteSupportFragment.
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(com.lleosoull.checkout.R.id.autocomplete_location) as AutocompleteSupportFragment?

        // Especifique os tipos de dados do local a serem retornados.
        autocompleteFragment!!.setPlaceFields(
            listOf(
                Place.Field.ID,
                Place.Field.LAT_LNG,
                Place.Field.NAME
            )
        )

        // Configure um PlaceSelectionListener para manipular a resposta.
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {

            override fun onPlaceSelected(place: Place) {
                Util.textName = place.name

                Util.latitudeDestino = place.latLng?.latitude
                Util.longitudeDestino = place.latLng?.longitude

                val changePage = Intent(this@MainActivity, SecondActivity::class.java)
                startActivity(changePage)
            }

            override fun onError(p0: Status) {
                Log.i("PlaceOnError", "Status ${p0.statusMessage}")
            }
        })
    }

    override fun onClick(view: View) {
        val id = view.id
        if (id == R.id.button_main) {
            Toast.makeText(this@MainActivity, "Por favor! Escolher um local", Toast.LENGTH_LONG)
                .show()
        }
    }


    override fun onResume() {
        super.onResume()

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            AlertDialog.Builder(this).apply {
                setMessage("O aplicativo Delivery precisa da sua localização, você aceita nós da permissão :")
                setTitle("Permissão")
                setPositiveButton("Sim") { d, i ->
                    // Se o usuário quiser, requere novamente permissão à funcionalidade
                    ActivityCompat.requestPermissions(
                        this@MainActivity,
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.INTERNET
                        ),
                        Context.CONTEXT_INCLUDE_CODE
                    )
                }
                setNegativeButton("Não") { d, i -> d.dismiss() }
            }.show()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Context.CONTEXT_INCLUDE_CODE -> {
                if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                    // Permissão foi concedida, já é possível usufruir da funcionalidade
                } else {
                }
            }
        }
    }
}
