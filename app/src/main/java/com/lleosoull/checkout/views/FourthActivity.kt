package com.lleosoull.checkout.views

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.lleosoull.checkout.R
import com.lleosoull.checkout.asynctask.CaminhoAsynctask
import com.lleosoull.checkout.asynctask.PuxarAsyncTask
import com.lleosoull.checkout.retrofit.RetrofitAll
import com.lleosoull.checkout.retrofit.api.DataFechamento
import com.lleosoull.checkout.retrofit.api.DataService
import com.lleosoull.checkout.retrofit.model.apiFechamento.FechamentoAnswer
import com.lleosoull.checkout.retrofit.model.apiFechamento.FechamentoQuestion
import com.lleosoull.checkout.retrofit.model.apiGoogle.LocationAnswer
import com.lleosoull.checkout.room.CartaoDB
import com.lleosoull.checkout.room.entity.CartaoModel
import com.lleosoull.checkout.util.Mask
import com.lleosoull.checkout.util.Util
import kotlinx.android.synthetic.main.activity_fourth.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FourthActivity : AppCompatActivity(), View.OnClickListener, CaminhoAsynctask {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fourth)
        button_cadastraCard.setOnClickListener(this)
        button_fouth.setOnClickListener(this)

        getRetrofitLocation()
        inicializacaoText()

    }

    override fun onResume() {
        super.onResume()

        PuxarAsyncTask(
            CartaoDB.getBancoDado(applicationContext)!!.cartaoDAO,
            this@FourthActivity
        ).execute()
    }

    override fun onClick(view: View) {
        val id = view.id
        if (id == R.id.button_cadastraCard) {
            startActivity(Intent(this, CardActivity::class.java))
        } else if (id == R.id.button_fouth) {
            if (Util.cartao_numero != null && Util.cartao_nome != null && Util.cartao_validade != null && Util.cartao_CVV != null) {

                getRetrofitFechamento()

            } else {
                Toast.makeText(
                    applicationContext,
                    "Cadastra seu cartão",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun getRetrofitFechamento() {

        val dataFechamento: DataFechamento = RetrofitAll().fechamentoService()
        val callback = dataFechamento.addFechamento(FechamentoQuestion())

        callback.enqueue(object : Callback<FechamentoAnswer> {
            override fun onFailure(call: Call<FechamentoAnswer>, t: Throwable) {
                Toast.makeText(
                    applicationContext,
                    "Verifique a sua internet",
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onResponse(
                call: Call<FechamentoAnswer>,
                response: Response<FechamentoAnswer>
            ) {
                if (response.isSuccessful) {

                    Util.message = response.body()?.message
                    Util.value = response.body()?.value

                    startActivity(Intent(this@FourthActivity, FinalActivity::class.java))
                }
            }
        })
    }

    fun getRetrofitLocation() {

        val desejo: String = "${Util.latitudeOrigin},${Util.longitudeOrigin}"
        val API_KEY = Util.API_KEY

        val dataService: DataService = RetrofitAll().dataService()
        val callback = dataService.getLocation(desejo, API_KEY)

        callback.enqueue(object : Callback<LocationAnswer> {
            override fun onFailure(call: Call<LocationAnswer>, t: Throwable) {

                Toast.makeText(
                    applicationContext,
                    "Verifique a sua internet",
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onResponse(
                call: Call<LocationAnswer>,
                response: Response<LocationAnswer>
            ) {
                if (response.isSuccessful) {
                    val articles = response.body()?.results?.get(0)?.formatted_address

                    text_enderecoUser.text = articles
                }
            }
        })
    }

    override fun bloco(usuarios: List<CartaoModel>) {

        if (!usuarios.isEmpty()) {
            Log.i("CartaoAsynctask", "nome cartao = ${usuarios[0].cartao_user}")

            Util.cartao_numero = usuarios[0].cartao_numero
            Util.cartao_nome = usuarios[0].cartao_user
            Util.cartao_validade = usuarios[0].cartao_validade
            Util.cartao_CVV = usuarios[0].cartao_cvv

            val numero : String = usuarios[0].cartao_numero.toString()
            val mask: String = Mask.maskCardNumber(numero , "**** **** **** ####")

            button_fouth.setBackgroundResource(R.drawable.wallpaper_selectd_button)
            mastercard_fourth.visibility = View.VISIBLE
            text_numDoCartao.text = mask
            view_pagamento.setBackgroundResource(R.drawable.line_unselected)
        }
    }

    fun inicializacaoText() {
        text_product_value.text = String.format("%.2f", Util.product_value)
        text_distance.text = String.format("%.2f", Util.distance)
        text_total_value.text = String.format("%.2f", Util.total_value)
        text_fee_value.text = String.format("%.2f", Util.fee_value)

        text_nameFourth.text = Util.textName
        text_descricaoFourth.text = Util.Adress
        text_pedidoInformadoFourth.text = Util.textProduto
    }
}
