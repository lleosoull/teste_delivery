package com.lleosoull.checkout.views


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.lleosoull.checkout.R
import com.lleosoull.checkout.retrofit.RetrofitAll
import com.lleosoull.checkout.retrofit.api.DataService
import com.lleosoull.checkout.retrofit.model.apiGoogle.LocationAnswer
import com.lleosoull.checkout.util.Util
import kotlinx.android.synthetic.main.activity_second.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SecondActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        getRetrofit()

        button_second.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        val id = view.id

        val editProduto: String? = edit_produto.text.toString()

        if (id == R.id.button_second) {
            if (editProduto != "") {

                Util.textProduto = editProduto
                startActivity(Intent(this@SecondActivity, ThirdActivity::class.java))

            } else {
                Toast.makeText(
                    this@SecondActivity,
                    "Favor inserir uma informação",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun getRetrofit() {

        val desejo: String = "${Util.latitudeDestino},${Util.longitudeDestino}"
        val API_KEY = Util.API_KEY

        val dataService: DataService = RetrofitAll().dataService()
        val callback = dataService.getLocation(desejo, API_KEY)

        callback.enqueue(object : Callback<LocationAnswer> {
            override fun onFailure(call: Call<LocationAnswer>, t: Throwable) {

                Toast.makeText(
                    applicationContext,
                    "Infelizmente ocorreu um erro!",
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onResponse(
                call: Call<LocationAnswer>,
                response: Response<LocationAnswer>
            ) {
                if (response.isSuccessful) {
                    val articles = response.body()?.results?.get(0)?.formatted_address

                    text_name.text = Util.textName
                    text_descricao.text = articles

                    //Endereço aonde desejo ir
                    Util.Adress = articles
                }
            }
        })
    }
}