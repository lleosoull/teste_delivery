package com.lleosoull.checkout.views

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.lleosoull.checkout.R
import com.lleosoull.checkout.asynctask.CaminhoAsynctask
import com.lleosoull.checkout.asynctask.InserirAsynctask
import com.lleosoull.checkout.asynctask.PuxarAsyncTask
import com.lleosoull.checkout.room.CartaoDB
import com.lleosoull.checkout.room.entity.CartaoModel
import com.lleosoull.checkout.util.Mask
import com.lleosoull.checkout.util.Util
import kotlinx.android.synthetic.main.activity_card.*

class CardActivity : AppCompatActivity(), View.OnClickListener, CaminhoAsynctask {

    var numeroDoCartao: String? = null
    var nomeDoCartao: String? = null
    var validade: String? = null
    var cartaoCVV: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)
        button_card.setOnClickListener(this)

        edit_numCartao.addTextChangedListener(Mask.maskValor("#### #### #### ####", edit_numCartao))
        edit_dataValidade.addTextChangedListener(Mask.maskValor("##/##", edit_dataValidade))

    }

    override fun onResume() {
        super.onResume()

        PuxarAsyncTask(
            CartaoDB.getBancoDado(applicationContext)!!.cartaoDAO,
            this@CardActivity
        ).execute()
    }

    override fun onClick(view: View) {
        val id = view.id

        numeroDoCartao = edit_numCartao.text.toString()
        nomeDoCartao = edit_nomeCartao.text.toString()
        validade = edit_dataValidade.text.toString()
        cartaoCVV = edit_CVV.text.toString()

        if (id == R.id.button_card) {
            if (numeroDoCartao != "") {
                if (nomeDoCartao != "") {
                    if (validade != "") {
                        if (cartaoCVV != "") {

                            salvandoInformacao()
                            finish()

                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Por favor inserir número do CVV",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Por favor inserir número de validade",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Nome do Cartão inválido",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "Numero do Cartão inválido",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun salvandoInformacao() {

        val intance = CartaoDB.getBancoDado(application)
        val cartaoDAO = intance!!.cartaoDAO
        val cartaoModel = CartaoModel()

        cartaoModel.cartao_user = nomeDoCartao
        cartaoModel.cartao_numero = numeroDoCartao
        cartaoModel.cartao_validade = validade
        cartaoModel.cartao_cvv = cartaoCVV

        InserirAsynctask(cartaoDAO, cartaoModel).execute(cartaoModel)
    }

    override fun bloco(usuarios: List<CartaoModel>) {
        if (!usuarios.isEmpty()) {

            edit_numCartao.hint = Util.cartao_numero
            edit_nomeCartao.hint = Util.cartao_nome
            edit_dataValidade.hint = Util.cartao_validade
            edit_CVV.hint = Util.cartao_CVV

            mastercard.visibility = View.VISIBLE
            button_card.setBackgroundResource(R.drawable.wallpaper_selectd_button)
        }
    }
}
