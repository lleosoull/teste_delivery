package com.lleosoull.checkout.views

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.lleosoull.checkout.R
import com.lleosoull.checkout.retrofit.RetrofitAll
import com.lleosoull.checkout.retrofit.api.DataCheckout
import com.lleosoull.checkout.retrofit.model.apiCheckout.CheckoutAnswer
import com.lleosoull.checkout.retrofit.model.apiCheckout.CheckoutQuestion
import com.lleosoull.checkout.util.LocationUtil
import com.lleosoull.checkout.util.Mask
import com.lleosoull.checkout.util.Util
import kotlinx.android.synthetic.main.activity_third.*
import retrofit2.Call
import retrofit2.Response

class ThirdActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        button_third.setOnClickListener(this)

        text_nameThird.text = Util.textName
        text_descricaoThird.text = Util.Adress
        text_pedidoInformado.text = Util.textProduto

        edit_valor.addTextChangedListener(Mask.maskValor("##.##", edit_valor))

        //pegando Localizacao do usuario
        LocationUtil.startLocationUpdates(this)

    }

    override fun onStop() {
        super.onStop()
        LocationUtil.stoplocationUpdates()
    }


    override fun onClick(view: View) {
        val id = view.id

        val editValor = edit_valor.text.toString()

        if (id == R.id.button_third) {
            if (editValor != "") {

                Util.valorInstimulado = editValor.toFloat()

                if (Util.latitudeOrigin != null && Util.longitudeOrigin != null){

                    getRetrofit()

                } else {
                    Toast.makeText(this@ThirdActivity, "Aguarde! Estamos pegando sua localização", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this@ThirdActivity, "Informação invalida", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun getRetrofit() {

        val dataCheckout: DataCheckout = RetrofitAll().checkoutService()
        val callback = dataCheckout.addCheckout(CheckoutQuestion())

        callback.enqueue(object : retrofit2.Callback<CheckoutAnswer> {
            override fun onFailure(call: Call<CheckoutAnswer>, t: Throwable) {
                Log.i("Location", "ERRO = ${t.localizedMessage}")

                Toast.makeText(this@ThirdActivity, "Estamos sem acesso a internet", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<CheckoutAnswer>,
                response: Response<CheckoutAnswer>
            ) {
                if (response.isSuccessful){
                    val articles = response.body()?.product_value

                    Util.product_value = response.body()?.product_value
                    Util.distance = response.body()?.distance
                    Util.total_value = response.body()?.total_value
                    Util.fee_value = response.body()?.fee_value

                    startActivity(Intent(this@ThirdActivity, FourthActivity::class.java))

                    Log.i("Location", "${response.body()?.product_value} | ${response.body()?.distance} | ${response.body()?.total_value}" +
                            " | ${response.body()?.fee_value}")
                }
            }

        })

    }
}
