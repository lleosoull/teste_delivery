package com.lleosoull.checkout.views

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.lleosoull.checkout.R
import com.lleosoull.checkout.util.Util
import kotlinx.android.synthetic.main.activity_final.*

class FinalActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final)
        button_final.setOnClickListener(this)

        reposta_shipp.text = Util.message
    }

    override fun onClick(view: View) {
        val id = view.id
        if (id == R.id.button_final){
            Toast.makeText(
                applicationContext,
                "Logo irá ter mais! Só aguarda!",
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
