package com.lleosoull.checkout.util

class Util {

    companion object {
        val API_KEY: String = "AIzaSyDC-2CFZlSmUUwKv_ONac_0cOI_iZXQoyY"

        var latitudeDestino: Double? = null
        var longitudeDestino: Double? = null
        var latitudeOrigin: Double? = null
        var longitudeOrigin: Double? = null

        var textName: String? = null
        var textProduto: String? = null

        var Adress: String? = null

        var valorInstimulado: Float? = null

        //resposta checkout
        var product_value: Double? = null
        var distance: Double? = null
        var total_value: Double? = null
        var fee_value: Double? = null

        //resposta fechamento
        var message: String? = null
        var value: Double? = null

        //resposta DAO
        var cartao_numero: String? = null
        var cartao_nome: String? = null
        var cartao_validade: String? = null
        var cartao_CVV: String? = null
    }
}