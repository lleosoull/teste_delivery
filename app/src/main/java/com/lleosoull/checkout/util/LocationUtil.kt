package com.lleosoull.checkout.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*

class LocationUtil {
    companion object {

        private lateinit var locationRequest: LocationRequest
        private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

        fun startLocationUpdates(context: Context) {

            locationRequest = LocationRequest()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = 2000
            locationRequest.fastestInterval = 1000


            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(locationRequest)
            val locationSettingsRequest = builder.build()

            val settingsClient = LocationServices.getSettingsClient(context)
            settingsClient.checkLocationSettings(locationSettingsRequest)

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {


                return
            }
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest, mLocationCallback,
                Looper.myLooper()
            )
        }

        private val mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {

                locationResult.lastLocation
                onLocationChanged(locationResult.lastLocation)
            }
        }

        fun onLocationChanged(location: Location) {
            Log.i("Location", "latitude : ${location.latitude} | longitude : ${location.longitude}")

            Util.latitudeOrigin = location.latitude
            Util.longitudeOrigin = location.longitude
        }


        fun stoplocationUpdates() {
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
        }
    }
}