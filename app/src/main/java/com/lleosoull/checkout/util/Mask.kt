package com.lleosoull.checkout.util

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText

class Mask {
    companion object {
        private fun replaceChars(cpfFull: String): String {
            return cpfFull
                .replace(".", "").replace("-", "")
                .replace("(", "").replace(")", "")
                .replace("/", "").replace(" ", "")
                .replace("*", "").replace(",", "")
        }

        var validacao: Boolean = false

        fun maskValor(mask: String, valor: EditText): TextWatcher {

            val textWatcher: TextWatcher = object : TextWatcher {

                var oUpdating: Boolean = false
                var nomeValor: String = ""

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun afterTextChanged(p0: Editable?) {}

                override fun onTextChanged(
                    charSequence: CharSequence,
                    começar: Int,
                    antes: Int,
                    contagem: Int
                ) {
                    Log.i(
                        "textWatcher",
                        "CharSequence : $charSequence | start : $começar | Before : $antes | count : $contagem"
                    )

                    val str: String = replaceChars(charSequence.toString())
                    var valueWithMask = ""

                    if (contagem == 0)
                        oUpdating = true

                    if (oUpdating) {
                        nomeValor = str
                        oUpdating = false
                        return
                    }

                    var i = 0
                    for (m: Char in mask.toCharArray()) {
                        if (m != '#' && str.length > nomeValor.length) {
                            valueWithMask += m
                            continue
                        }
                        try {
                            valueWithMask += str.get(i)
                        } catch (e: Exception) {
                            break
                        }
                        i++
                    }

                    oUpdating = true
                    valor.setText(valueWithMask)
                    valor.setSelection(valueWithMask.length)
                }
            }
            return textWatcher
        }

        fun maskCardNumber(cardNumber: String, mask: String): String {

            var index = 3
            val maskedNumber = StringBuilder()
            for (i in 0 until mask.length) {
                val c = mask[i]
                if (c == '#') {
                    maskedNumber.append(cardNumber[index])
                    index++
                } else if (c == '*') {
                    maskedNumber.append(c)
                    index++
                } else {
                    maskedNumber.append(c)
                }
            }
            return maskedNumber.toString()
        }

    }
}